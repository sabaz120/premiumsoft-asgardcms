<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/premiumsoft'], function (Router $router) {
    $router->bind('databasehost', function ($id) {
        return app('Modules\Premiumsoft\Repositories\DatabaseHostRepository')->find($id);
    });
    $router->get('databasehosts', [
        'as' => 'admin.premiumsoft.databasehost.index',
        'uses' => 'DatabaseHostController@index',
        'middleware' => 'can:premiumsoft.databasehosts.index'
    ]);



    $router->get('databasehosts/create', [
        'as' => 'admin.premiumsoft.databasehost.create',
        'uses' => 'DatabaseHostController@create',
        'middleware' => 'can:premiumsoft.databasehosts.create'
    ]);
    $router->post('databasehosts', [
        'as' => 'admin.premiumsoft.databasehost.store',
        'uses' => 'DatabaseHostController@store',
        'middleware' => 'can:premiumsoft.databasehosts.create'
    ]);
    $router->get('databasehosts/{databasehost}/edit', [
        'as' => 'admin.premiumsoft.databasehost.edit',
        'uses' => 'DatabaseHostController@edit',
        'middleware' => 'can:premiumsoft.databasehosts.edit'
    ]);
    $router->put('databasehosts/{databasehost}', [
        'as' => 'admin.premiumsoft.databasehost.update',
        'uses' => 'DatabaseHostController@update',
        'middleware' => 'can:premiumsoft.databasehosts.edit'
    ]);
    $router->delete('databasehosts/{databasehost}', [
        'as' => 'admin.premiumsoft.databasehost.destroy',
        'uses' => 'DatabaseHostController@destroy',
        'middleware' => 'can:premiumsoft.databasehosts.destroy'
    ]);

    $router->get('priceadjustment', [
        'as' => 'admin.premiumsoft.priceadjustment.index',
        'uses' => 'PriceAdjustmentController@index',
        'middleware' => 'can:premiumsoft.priceadjustment.index'
    ]);

    $router->get('priceadjustment/create', [
        'as' => 'admin.premiumsoft.priceadjustment.create',
        'uses' => 'PriceAdjustmentController@create',
        'middleware' => 'can:premiumsoft.priceadjustment.create'
    ]);

    $router->post('priceadjustment', [
        'as' => 'admin.premiumsoft.priceadjustment.store',
        'uses' => 'PriceAdjustmentController@store',
        'middleware' => 'can:premiumsoft.priceadjustment.create'
    ]);

    /*
      ->Price Update API'S
    */

    //View to price update
    $router->get('priceupdate', [
        'as' => 'admin.premiumsoft.price.update.index',
        'uses' => 'PriceUpdateController@index',
        //'middleware' => 'can:premiumsoft.databasehosts.index'
    ]);
    //Api find article by code or name
    $router->get('getArticle', [
      'as' => 'admin.premiumsoft.article.show',
      'uses' => 'PriceUpdateController@getArticle',
      //'middleware' => 'can:premiumsoft.databasehosts.index'
    ]);
    //Api update prices
    $router->post('priceupdate', [
        'as' => 'admin.premiumsoft.price.update.update',
        'uses' => 'PriceUpdateController@update',
        //'middleware' => 'can:premiumsoft.databasehosts.index'
    ]);
    //Api update code in multiple tables
    $router->post('priceupdatecode', [
        'as' => 'admin.premiumsoft.price.update.code.update',
        'uses' => 'PriceUpdateController@updateCode',
        //'middleware' => 'can:premiumsoft.databasehosts.index'
    ]);

    /*
      ->Price Update API'S
    */

    $router->get('updateproductscodes', [
        'as' => 'admin.premiumsoft.updateproductscode.index',
        'uses' => 'UpdateProductsCodeController@index',
        'middleware' => 'can:premiumsoft.updateproductscodes.index'
    ]);
    $router->get('updateproductscodes/create', [
        'as' => 'admin.premiumsoft.updateproductscode.create',
        'uses' => 'UpdateProductsCodeController@create',
        'middleware' => 'can:premiumsoft.updateproductscodes.create'
    ]);


    // DEMO
    $router->get('client', [
        'as' => 'admin.premiumsoft.client.index',
        'uses' => 'ClientControllers@index',
        'middleware' => 'can:premiumsoft.updateproductscodes.index'
    ]);

    $router->get('orders', [
        'as' => 'admin.premiumsoft.orders.index',
        'uses' => 'ClientControllers@indexOperti',
        'middleware' => 'can:premiumsoft.updateproductscodes.index'
    ]);

// append


});

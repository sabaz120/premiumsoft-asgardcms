<?php

namespace Modules\Premiumsoft\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Premiumsoft\Entities\DatabaseHost;
use Modules\Premiumsoft\Entities\Articulo;
use Modules\Premiumsoft\Http\Requests\CreateDatabaseHostRequest;
use Modules\Premiumsoft\Http\Requests\UpdateDatabaseHostRequest;
use Modules\Premiumsoft\Repositories\DatabaseHostRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DatabaseHostController extends AdminBaseController
{
    /**
     * @var DatabaseHostRepository
     */
    private $databasehost;

    public function __construct(DatabaseHostRepository $databasehost)
    {
        parent::__construct();

        $this->databasehost = $databasehost;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $databasehosts = $this->databasehost->all();

        return view('premiumsoft::admin.databasehosts.index', compact('databasehosts'));
    }

    public function test(){
        $arrayIds=[3];
        $databases=DatabaseHost::find($arrayIds);
        foreach($databases as $database){
            \Config::set("database.connections.premium", [
                "driver" => $database->driver,
                "host" => $database->ip,
                "database" => $database->database,
                "username" => $database->username,
                "password" => $database->password
                ]);
            dd(Articulo::all());
        }//foreach

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('premiumsoft::admin.databasehosts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDatabaseHostRequest $request
     * @return Response
     */
    public function store(CreateDatabaseHostRequest $request)
    {
        $this->databasehost->create($request->all());

        return redirect()->route('admin.premiumsoft.databasehost.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('premiumsoft::databasehosts.title.databasehosts')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  DatabaseHost $databasehost
     * @return Response
     */
    public function edit(DatabaseHost $databasehost)
    {
        return view('premiumsoft::admin.databasehosts.edit', compact('databasehost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DatabaseHost $databasehost
     * @param  UpdateDatabaseHostRequest $request
     * @return Response
     */
    public function update(DatabaseHost $databasehost, UpdateDatabaseHostRequest $request)
    {
        $this->databasehost->update($databasehost, $request->all());

        return redirect()->route('admin.premiumsoft.databasehost.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('premiumsoft::databasehosts.title.databasehosts')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DatabaseHost $databasehost
     * @return Response
     */
    public function destroy(DatabaseHost $databasehost)
    {
        $this->databasehost->destroy($databasehost);

        return redirect()->route('admin.premiumsoft.databasehost.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('premiumsoft::databasehosts.title.databasehosts')]));
    }
}

<?php

namespace Modules\Premiumsoft\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Premiumsoft\Entities\DatabaseHost;
class UpdateProductsCodeController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $database = DatabaseHost::all();
        return view('premiumsoft::admin.updateproductscodes.edit', compact('database'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('premiumsoft::admin.updateproductscodes.create');
    }
}

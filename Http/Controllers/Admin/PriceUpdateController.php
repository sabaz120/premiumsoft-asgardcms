<?php

namespace Modules\Premiumsoft\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//Entities
use Modules\Premiumsoft\Entities\DatabaseHost;
use Modules\Premiumsoft\Entities\Articulo;
use Modules\Premiumsoft\Entities\Artiprov;
use Modules\Premiumsoft\Entities\Existenc;
use Modules\Premiumsoft\Entities\Kardex;
use Modules\Premiumsoft\Entities\Ajustemv;
use Modules\Premiumsoft\Entities\Cargodet;
use Modules\Premiumsoft\Entities\Devolmv;
use Modules\Premiumsoft\Entities\Gastarmv;
use Modules\Premiumsoft\Entities\Opermv;
//Request
use Modules\Premiumsoft\Http\Requests\UpdatePriceRequest;
//Controller
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
//Repositories
use Modules\Premiumsoft\Repositories\DatabaseHostRepository;

class PriceUpdateController extends AdminBaseController
{
    /**
     * @var DatabaseHostRepository
     */
    private $databasehost;

    public function __construct(DatabaseHostRepository $databasehost)
    {
        parent::__construct();

        $this->databasehost = $databasehost;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $databasehosts = $this->databasehost->all();

        return view('premiumsoft::admin.databasehosts.index', compact('databasehosts'));
    }

    public function getArticle(Request $request){
      //Function to find article by specific connection
      //Sabas was here - DONT TOUCH THIS
        $database=DatabaseHost::find($request->database_id);
         \Config::set("database.connections.".$database->name, [
            "driver" => $database->driver,
            "host" => $database->ip,
            "port" => $database->port,
            "database" => $database->database,
            "username" => $database->username,
            "password" => $database->password
          ]);
        $articles=Articulo::on($database->name);
        $filters=isset($request->filters) ? $request->filters : (object)[];
        $filters=json_decode(json_encode($filters));
        if(isset($filters->code)){
          $articles->where('codigo', 'like', "%$filters->code%");
        }
        if(isset($filters->name)){
          $articles->where('nombre', 'like', "%$filters->name%");
        }
        $articles=$articles->get();
        // $article=Articulo::where('codigo',$request->code)->get();
        return response()->json(['data'=>$articles]);
    }//

    public function update(Request $request){
      //Function to update price in multiple connections
      try {
        //Recibe arrayIds, code .
        $data=$request->all();
        $arrayIds=$request->arrayIds;
        $code=$data['code'];
        // $arrayIds=[3];
        $databases=DatabaseHost::find($arrayIds);
        unset($data['arrayIds']);
        unset($data['code']);
        foreach($databases as $database){
          \Config::set("database.connections.".$database->name, [
            "driver" => $database->driver,
            "host" => $database->ip,
            "port" => $database->port,
            "database" => $database->database,
            "username" => $database->username,
            "password" => $database->password
          ]);
          $arti=Articulo::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
        }//foreach
        return response()->json(["data"=>""],200);
      } catch (\Exception $e) {
        return response()->json(["error"=>$e->getMessage(),"line"=>$e->getLine()],500);
      }

    }//update

    public function updateCode(Request $request){
      //Function to update code in multiple connections & multiple tables
      try {
        //Recibe arrayIds, code .
        $data=$request->all();
        $arrayIds=$request->arrayIds;
        $code=$data['codeSearch'];
        // $arrayIds=[3];
        $databases=DatabaseHost::find($arrayIds);
        unset($data['arrayIds']);
        unset($data['codeSearch']);
        foreach($databases as $database){
          //Sabas was here - DONT TOUCH THIS
          \Config::set("database.connections.".$database->name, [
            "driver" => $database->driver,
            "host" => $database->ip,
            "port" => $database->port,
            "database" => $database->database,
            "username" => $database->username,
            "password" => $database->password
          ]);
          Articulo::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Artiprov::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Existenc::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Kardex::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Ajustemv::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Cargodet::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Devolmv::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Gastarmv::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
          Opermv::on($database->name)->where('codigo', 'like', "%$code%")->update($data);
        }//foreach
        return response()->json(["data"=>""],200);
      } catch (\Exception $e) {
        return response()->json(["error"=>$e->getMessage(),"line"=>$e->getLine()],500);
      }

    }//update


}

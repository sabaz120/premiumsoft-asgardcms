<?php

namespace Modules\Premiumsoft\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Premiumsoft\Entities\CliempreWeb;
use Modules\Premiumsoft\Entities\OpertiWeb;
use Modules\Premiumsoft\Entities\OpermvWeb;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ClientControllers extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $clientes = CliempreWeb::all();
        return view('premiumsoft::admin.client.index',compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('premiumsoft::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('premiumsoft::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('premiumsoft::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function indexOperti()
    {
        $orders = OpertiWeb::all();
        return view('premiumsoft::admin.orders.index',compact('orders'));
    }
}

<?php

namespace Modules\Premiumsoft\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Premiumsoft\Entities\Articulo;
use Modules\Premiumsoft\Entities\Artiprov;
use Modules\Premiumsoft\Entities\Existenc;
use Modules\Premiumsoft\Entities\Kardex;
use Modules\Premiumsoft\Entities\Ajustemv;
use Modules\Premiumsoft\Entities\Cargodet;
use Modules\Premiumsoft\Entities\Devolmv;
use Modules\Premiumsoft\Entities\Gastarmv;
use Modules\Premiumsoft\Entities\Opermv;
use Modules\Premiumsoft\Entities\OpermvWeb;
use Modules\Premiumsoft\Entities\Operti;
use Modules\Premiumsoft\Entities\OpertiWeb;
use Modules\Premiumsoft\Entities\Cliempre;
use Modules\Premiumsoft\Entities\CliempreWeb;
use Modules\Premiumsoft\Entities\Cargoenc;
use DB;
use Carbon\Carbon;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('premiumsoft::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('premiumsoft::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('premiumsoft::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('premiumsoft::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function getArticles(Request $request){
         $article=Articulo::with('grupo')->get();
         foreach ($article as &$key) {
           // $prueba = explode('\\',$key->rutafoto); #windows;
           $prueba = explode('/',$key->rutafoto); #linux;
           $key->rutafoto = $prueba[count($prueba)-1];
           // $key->precioSinFormato = $key->precio1;
           // $key->precio1 = number_format($key->precio1, 2, ',', '.');

         }

        return response()->json(['data'=>$article]);
    }//

    public function showCart()
    {
        $operti = Operti::all();
        return view('premiumsoft::frontend.partials.cart',compact('operti'));

    }

    public function showAllProducts()
    {
        return view('premiumsoft::frontend.partials.products');
    }
    public function showBlog()
    {
        return view('premiumsoft::frontend.partials.blog');
    }

    public function apiOperti(){
      $operti = Operti::all();
      return response()->json(["data"=>$operti],200);

    }


    public function storeCart(Request $request){

      try {
        DB::beginTransaction();

        $carts=json_decode(json_encode($request->articulos));
        foreach($carts as $cart){
          $montoTotal=0;
          $montoTotal=$cart->precio1*$cart->cantidad;


          // Kardex::firstOrCreate([
          //   "documento"=>$request->documento,
          //   "codigo"=>$cart->codigo,
          // ],[
          //   "id_empresa"=>'001000',
          //   "agencia"=>'001',
          //   "documento"=>$request->documento,
          //   "codigo"=>$cart->codigo,
          //   "hora"=>Carbon::now()->format('H:i:s'),
          //   "grupo"=>$cart->grupo->codigo,
          //   "origen"=>'ESP',
          //   "concepto"=>'Factura por compra # 01010101',
          //   "cantidad"=>$cart->cantidad,
          //   "estacion"=>'000',
          //   "almacen"=>'01',
          //   "sumaresta"=>'2',
          //   "emisor"=>'PREMIUM',
          //   "fecha"=>Carbon::now()->format('Y-m-d'),
          //   "aux1"=>'0.0000000',
          //   "aux2"=>'0.0000000',
          //   "aux3"=>'0.0000000',
          //   "tipo_resta"=>'0',
          //   "pid"=>' ',
          //   "idvalidacion"=>NULL,
          //   "costo"=>$cart->precio1,
          //   "costoprom"=>$cart->precio1,
          //   "exist_ant"=>$cart->existencia,
          //   "costoprompbl"=>'0.0000000',
          //   "costoex_adic"=>'0.0000000',
          //   "incrcosprom"=>'0.0000000',
          // ]);

          if ($cart->subgrupo == '' || $cart->subgrupo ==null) {
            $cart->subgrupo = "00";
          }
          // Cargodet::firstOrCreate([
          //   "documento"=>$request->documento,
          //   "codigo"=>$cart->codigo,
          // ],[
          //   "id_empresa"=>'001000',
          //   "agencia"=>'001',
          //   "documento"=>$request->documento, //El campo documento es el valor del campo documento que se guardo en el kardex (el que se creo en la vista)
          //   "status"=>'0',
          //   "ordentrab"=>' ',
          //   "codigo"=>$cart->codigo,
          //   "idcampo"=>' ',
          //   "nombre"=>$cart->nombre,
          //   "grupo"=>$cart->grupo->codigo,
          //   "subgrupo"=>$cart->subgrupo,
          //   "proceso"=>'1',
          //   "cantidad"=>$cart->cantidad,
          //   "tallas"=>' ',
          //   "colores"=>' ',
          //   "fecha"=>Carbon::now()->format('Y-m-d'),
          //   "almacen"=>'01',
          //   "emisor"=>'PREMIUM',
          //   "estacion"=>'000',
          //   "estacion"=>'000',
          //   "usaserial"=>'2',
          //   "seriales"=>NULL,
          //   "ubicacion"=>' ',
          //   "precio"=>$cart->precio1,
          //   "fechanul"=>Carbon::now()->format('Y-m-d'),
          //   "useranul"=>' ',
          //   "motivoanul"=>' ',
          //   "lote"=>' ',
          //   "costo"=>$cart->costo,
          //   "costopromfecha"=>$cart->costo,
          //   "factor"=>'1.0000000',
          //   "tipo"=>'0',
          // ]);

          Opermv::firstOrCreate([
            "documento"=>$request->documento,
            "codigo"=>$cart->codigo,
          ],[
            "id_empresa"=>'001000',
            "agencia"=>'001',
            "tipodoc"=>'ESP',
            "documento"=>$request->documento, //El campo documento es el valor del campo documento que se guardo en el kardex (el que se creo en la vista)
            "grupo"=>$cart->grupo->codigo,
            "subgrupo"=>$cart->subgrupo,
            "origen"=>'1',
            "codigo"=>$cart->codigo,
            "codhijo"=>' ',
            "pid"=>' ',
            "nombre"=>$cart->nombre,
            "costounit"=>$cart->costo,
            "preciounit"=>$cart->precio1,
            "diferencia"=>'0.0000000',
            "dsctounit"=>'0.0000000',
            "coddescuento"=>' ',
            "dsctoprc"=>'0.0000000',
            "dsctoendm"=>'0.0000000',
            "dsctomtolinea"=>'0.0000000',
            "dsctoendp"=>'0.0000000',
            "preciofin"=>$cart->precio1,
            "prcrecargo"=>'0.0000000',
            "recargounit"=>'0.0000000',
            "preciooriginal"=>$cart->precio1,
            "cantidad"=>$cart->cantidad,
            "cntdevuelt"=>'0.0000000',
            "unidevuelt"=>'0.0000000',
            "cntentrega"=>'0.0000000',
            "tallas"=>' ',
            "colores"=>' ',
            "montoneto"=>$montoTotal ,//ES EL CALCULO DEL PRECIO * CANTIDAD,
            "montototal"=>$montoTotal, //ES EL CALCULO DEL PRECIO * CANTIDAD,
            "almacen"=>'01',
            "proveedor"=>'PREMIUM',
            "fechadoc"=>Carbon::now()->format('Y-m-d'),
            "impuesto1"=>'0.0000000',
            "impuesto2"=>'0.0000000',
            "impuesto3"=>'0.0000000',
            "impuesto4"=>'0.0000000',
            "impuesto5"=>'0.0000000',
            "impuesto6"=>'0.0000000',
            "timpueprc"=>'0.0000000',
            "impu_mto"=>'0.0000000',
            "comision"=>'0.0000000',
            "comisprc"=>'0.0000000',
            "vendedor"=>'01',
            "emisor"=>'PREMIUM',
            "usaserial"=>'2',
            "tipoprecio"=>'1',
            "unidad"=>' ',
            "agrupado"=>'2',
            "cntagrupada"=>'0.0000000',
            "seimporto"=>'1',
            "desdeimpor"=>' ',
            "notas"=>' ',
            "oferta"=>'0',
            "compuesto"=>'2',
            "usaexist"=>'1',
            "aux1"=>'0.0000000',
            "estacion"=>'000',
            "estacionfac"=>' ',
            "clasifica"=>'0',
            "cuentacont"=>NULL,
            "placa"=>NULL,
            "placaod"=>NULL,
            "udinamica"=>'1',
            "cantref"=>'0.0000000',
            "unidadref"=>' ',
            "baseimpo1"=>'0.0000000',
            "baseimpo2"=>'0.0000000',
            "baseimpo3"=>'0.0000000',
            "lote"=>' ',
            "imp_nacional"=>'0.0000000',
            "imp_producc"=>'0.0000000',
            "se_imprimio"=>'0',
            "ofertaconvenio"=>'0.0000000',
            "ofertaconvenio"=>'0.0000000',
            "cod_servidor"=>' ',
            "prc_comi_servidor"=>'0.0000000',
            "mto_comi_servidor"=>'0.0000000',
            "checkin"=>' ',
            "habi_nro"=>' ',
            "idoferta"=>' ',
            "agencian"=>' ',
            "tipodocant"=>' ',
            "documant"=>' ',
            "uemisorant"=>' ',
            "estacioant"=>' ',
            "fechadocant"=>Carbon::now()->format('Y-m-d'),
            "fechayhora"=>Carbon::now(),
            "frog"=>'0',
            "documentolocal"=>' ',
            "linbloq"=>'0',
            "enviado_kmonitor"=>'0',
            "se_guardo"=>'0',

          ]);


          OpermvWeb::firstOrCreate([
            "documento"=>$request->documento,
            "codigo"=>$cart->codigo,
          ],[
            "id_empresa"=>'001000',
            "agencia"=>'001',
            "tipodoc"=>'ESP',
            "documento"=>$request->documento, //El campo documento es el valor del campo documento que se guardo en el kardex (el que se creo en la vista)
            "grupo"=>$cart->grupo->codigo,
            "subgrupo"=>$cart->subgrupo,
            "origen"=>'1',
            "codigo"=>$cart->codigo,
            "codhijo"=>' ',
            "pid"=>' ',
            "nombre"=>$cart->nombre,
            "costounit"=>$cart->costo,
            "preciounit"=>$cart->precio1,
            "diferencia"=>'0.0000000',
            "dsctounit"=>'0.0000000',
            "coddescuento"=>' ',
            "dsctoprc"=>'0.0000000',
            "dsctoendm"=>'0.0000000',
            "dsctomtolinea"=>'0.0000000',
            "dsctoendp"=>'0.0000000',
            "preciofin"=>$cart->precio1,
            "prcrecargo"=>'0.0000000',
            "recargounit"=>'0.0000000',
            "preciooriginal"=>$cart->precio1,
            "cantidad"=>$cart->cantidad,
            "cntdevuelt"=>'0.0000000',
            "unidevuelt"=>'0.0000000',
            "cntentrega"=>'0.0000000',
            "tallas"=>' ',
            "colores"=>' ',
            "montoneto"=>$montoTotal ,//ES EL CALCULO DEL PRECIO * CANTIDAD,
            "montototal"=>$montoTotal, //ES EL CALCULO DEL PRECIO * CANTIDAD,
            "almacen"=>'01',
            "proveedor"=>'PREMIUM',
            "fechadoc"=>Carbon::now()->format('Y-m-d'),
            "impuesto1"=>'0.0000000',
            "impuesto2"=>'0.0000000',
            "impuesto3"=>'0.0000000',
            "impuesto4"=>'0.0000000',
            "impuesto5"=>'0.0000000',
            "impuesto6"=>'0.0000000',
            "timpueprc"=>'0.0000000',
            "impu_mto"=>'0.0000000',
            "comision"=>'0.0000000',
            "comisprc"=>'0.0000000',
            "vendedor"=>'01',
            "emisor"=>'PREMIUM',
            "usaserial"=>'2',
            "tipoprecio"=>'1',
            "unidad"=>NULL,
            "agrupado"=>'2',
            "cntagrupada"=>'0.0000000',
            "seimporto"=>'1',
            "desdeimpor"=>' ',
            "notas"=>' ',
            "oferta"=>'0',
            "compuesto"=>'2',
            "usaexist"=>'1',
            "aux1"=>'0.0000000',
            "estacion"=>'000',
            "estacionfac"=>' ',
            "clasifica"=>'0',
            "cuentacont"=>NULL,
            "placa"=>NULL,
            "placaod"=>NULL,
            "udinamica"=>'1',
            "cantref"=>'0.0000000',
            "unidadref"=>' ',
            "baseimpo1"=>'0.0000000',
            "baseimpo2"=>'0.0000000',
            "baseimpo3"=>'0.0000000',
            "lote"=>' ',
            "imp_nacional"=>'0.0000000',
            "imp_producc"=>'0.0000000',
            "se_imprimio"=>'0',
            "ofertaconvenio"=>'0.0000000',
            "ofertaconvenio"=>'0.0000000',
            "cod_servidor"=>' ',
            "prc_comi_servidor"=>'0.0000000',
            "mto_comi_servidor"=>'0.0000000',
            "checkin"=>' ',
            "habi_nro"=>' ',
            "idoferta"=>' ',
            "agencian"=>' ',
            "tipodocant"=>' ',
            "documant"=>' ',
            "uemisorant"=>' ',
            "estacioant"=>' ',
            "fechadocant"=>Carbon::now()->format('Y-m-d'),
            "fechayhora"=>Carbon::now(),
            "frog"=>'0',
            "documentolocal"=>' ',
            "linbloq"=>'0',
            "enviado_kmonitor"=>'0',
            "se_guardo"=>'0',

          ]);

        }//foreach carts

         $cliempre=Cliempre::updateOrCreate(
            [
              'codigo'=>$request->cedula,
              'cedula'=>$request->cedula,
            ],
            [
              'id_empresa'=>'001000',
              'agencia'=>'001',
              'codigo'=>$request->cedula,
              'nombre'=>$request->nombre,
              'cedula'=>$request->cedula,
              'nrorif'=>$request->cedula,
              'direccion'=>$request->direccion,
              'telefonos'=>$request->telefono,
              'fecha'=>Carbon::now()->format('Y-m-d'),
              'fechanac'=>Carbon::now()->format('Y-m-d'),
              'fechanac1'=>Carbon::now()->format('Y-m-d'),
              'fechanac2'=>Carbon::now()->format('Y-m-d'),
              'fechanac3'=>Carbon::now()->format('Y-m-d'),
              'fechanac4'=>Carbon::now()->format('Y-m-d'),
              'fechanac5'=>Carbon::now()->format('Y-m-d'),
            ]
          );

          CliempreWeb::updateOrCreate(
            [
              'codigo'=>$request->cedula,
              'cedula'=>$request->cedula,
            ],
            [
              'codigo'=>$request->cedula,
              'nombre'=>$request->nombre,
              'cedula'=>$request->cedula,
              'nrorif'=>$request->cedula,
              'direccion'=>$request->direccion,
              'telefonos'=>$request->telefono,
              'fecha'=>Carbon::now()->format('Y-m-d'),
              'fechanac'=>Carbon::now()->format('Y-m-d'),
              'fechanac1'=>Carbon::now()->format('Y-m-d'),
              'fechanac2'=>Carbon::now()->format('Y-m-d'),
              'fechanac3'=>Carbon::now()->format('Y-m-d'),
              'fechanac4'=>Carbon::now()->format('Y-m-d'),
              'fechanac5'=>Carbon::now()->format('Y-m-d'),
            ]
          );




          Operti::firstOrCreate([
            "documento"=>$request->documento,
            "codcliente"=>$request->cedula,
          ],[
            "id_empresa"=>'001000',
            "agencia"=>'001',
            "tipodoc"=>'ESP',
            "subtipodoc"=>' ',
            "moneda"=>'000',
            "documento"=>'02020202',
            "codcliente"=>$request->cedula,
            "nombrecli"=>$request->nombre,
            "contacto"=>$request->nombre,
            "rif"=>$request->cedula,
            "nit"=>' ',
            "direccion"=>$request->direccion,
            "tipoprecio"=>'1',
            "orden"=>'01',
            "emision"=>Carbon::now()->format('Y-m-d'),
            "recepcion"=>Carbon::now()->format('Y-m-d'),
            "vence"=>Carbon::now()->addDays(2)->format('Y-m-d'),
            "ult_interes"=>Carbon::now()->format('Y-m-d'),
            "fechacrea"=>Carbon::now()->format('Y-m-d'),
            "totcosto"=>$request->totalCarrito, //total del carrito,
            "totcomi"=>'0.0000000',
            "totbruto"=>$request->totalCarrito, //total del carrito,
            "totalfinal"=>$request->totalCarrito, //total del carrito,
            "totimpuest"=>'0.0000000',
            "totdescuen"=>'0.0000000',
            "impuesto1"=>'0.0000000',
            "impuesto2"=>'0.0000000',
            "impuesto3"=>'0.0000000',
            "impuesto4"=>'0.0000000',
            "impuesto5"=>'0.0000000',
            "impuesto6"=>'0.0000000',
            "recargos"=>'0.0000000',
            "dsctoend1"=>'0.0000000',
            "dsctoend2"=>'0.0000000',
            "dsctolinea"=>'0.0000000',
            "notas"=>' ',
            "estatusdoc"=>'1',
            "ultimopag"=>Carbon::now()->format('Y-m-d'),
            "diascred"=>'0',
            "vendedor"=>'01',
            "factorcamb"=>'1.0000000',
            "multi_div"=>'0',
            "factorreferencial"=>'0.0000000',
            "fechanul"=>Carbon::now()->format('Y-m-d'),
            "uanulador"=>' ',
            "uemisor"=>'PREMIUM',
            "estacion"=>'000',
            "sinimpuest"=>'0.0000000',
            "almacen"=>'01',
            "sector"=>'01',
            "formafis"=>'1',
            "al_libro"=>'1',
            "codigoret"=>'1',
            "retencion"=>'0.0000000',
            "aux1"=>'0.0000000',
            "aux2"=>'0.0000000',
            "aplicadoa"=>'00000001   ',
            "referencia"=>' ',
            "refmanual"=>' ',
            "doc_class_id"=>' ',
            "operac"=>'02',
            "motanul"=>' ',
            "seimporto"=>'1',
            "dbcr"=>'1',
            "horadocum"=>Carbon::now()->format('H:i'),
            "ampm"=>'2',
            "tranferido"=>'0',
            "procedecre"=>'0',
            "entregado"=>'0',
            "vuelto"=>'0.0000000',
            "integrado"=>'0',
            "escredito"=>'0',
            "seq_nodo"=>'0000000001',
            "tipo_nc"=>' ',
            "porbackord"=>'0',
            "chequedev"=>'0',
            "ordentrab"=>' ',
            "compcont"=>' ',
            "planillacob"=>' ',
            "nodoremoto"=>' ',
            "turno"=>' ',
            "codvend_a"=>' ',
            "codvend_b"=>' ',
            "codvend_C"=>' ',
            "codvend_d"=>' ',
            "baseimpo1"=>'0.0000000',
            "baseimpo2"=>'0.0000000',
            "baseimpo3"=>'0.0000000',
            "iddocumento"=>' ',
            "imp_nacional"=>'0.0000000',
            "imp_producc"=>'0.0000000',
            "retencioniva"=>'0.0000000',
            "fechayhora"=>Carbon::now(),
            "tipopersona"=>'0',
            "idvalidacion"=>' ',
            "nosujeto"=>'0',
            "serialprintf"=>' ',
            "documentofiscal"=>'...',
            "numeroz"=>' ',
            "ubica"=>' ',
            "usa_despacho"=>'0',
            "despachador"=>' ',
            "despacho_nro"=>' ',
            "checkin"=>' ',
            "nureserva"=>' ',
            "grandocnum"=>' ',
            "agenciant"=>' ',
            "tipodocant"=>' ',
            "documant"=>' ',
            "uemisorant"=>' ',
            "estacioant"=>' ',
            "emisionant"=>Carbon::now()->format('Y-m-d'),
            "fchyhrant"=>Carbon::now(),
            "frog"=>'0',
            "apa_nc"=>'0',
            "documentolocal"=>'...',
            "comanda_movil"=>'0',
            "comanda_kmonitor"=>'0',
            "para_llevar"=>'0',
            "notimbrar"=>'0',
            "antipo"=>' ',
            "antdoc"=>' ',
            "xrequest"=>' ',
            "xresponse"=>' ',
            "parcialidad"=>'0',
            "cedcompra"=>' ',
            "subcodigo"=>' ',
            "contingencia"=>'0',
            "precta_movil"=>'0',
            "tipodocfiscal"=>'0',
            "cprefijoserie"=>' ',
            "cserie"=>' ',
            "serieincluyeimpuesto"=>'0',
            "serieauto"=>'0',
            "opemail"=>' ',
            "refmanual2"=>' ',
          ]);

          OpertiWeb::firstOrCreate([
            "documento"=>$request->documento,
            "codcliente"=>$request->cedula,
          ],[
            "id_empresa"=>'001000',
            "agencia"=>'001',
            "tipodoc"=>'ESP',
            "subtipodoc"=>' ',
            "moneda"=>'000',
            "documento"=>'02020202',
            "codcliente"=>$request->cedula,
            "nombrecli"=>$request->nombre,
            "contacto"=>$request->nombre,
            "rif"=>$request->cedula,
            "nit"=>' ',
            "direccion"=>$request->direccion,
            "tipoprecio"=>'1',
            "orden"=>'01',
            "emision"=>Carbon::now()->format('Y-m-d'),
            "recepcion"=>Carbon::now()->format('Y-m-d'),
            "vence"=>Carbon::now()->addDays(2)->format('Y-m-d'),
            "ult_interes"=>Carbon::now()->format('Y-m-d'),
            "fechacrea"=>Carbon::now()->format('Y-m-d'),
            "totcosto"=>$request->totalCarrito, //total del carrito,
            "totcomi"=>'0.0000000',
            "totbruto"=>$request->totalCarrito, //total del carrito,
            "totalfinal"=>$request->totalCarrito, //total del carrito,
            "totimpuest"=>'0.0000000',
            "totdescuen"=>'0.0000000',
            "impuesto1"=>'0.0000000',
            "impuesto2"=>'0.0000000',
            "impuesto3"=>'0.0000000',
            "impuesto4"=>'0.0000000',
            "impuesto5"=>'0.0000000',
            "impuesto6"=>'0.0000000',
            "recargos"=>'0.0000000',
            "dsctoend1"=>'0.0000000',
            "dsctoend2"=>'0.0000000',
            "dsctolinea"=>'0.0000000',
            "notas"=>' ',
            "estatusdoc"=>'1',
            "ultimopag"=>Carbon::now()->format('Y-m-d'),
            "diascred"=>'0',
            "vendedor"=>'01',
            "factorcamb"=>'1.0000000',
            "multi_div"=>'0',
            "factorreferencial"=>'0.0000000',
            "fechanul"=>Carbon::now()->format('Y-m-d'),
            "uanulador"=>' ',
            "uemisor"=>'PREMIUM',
            "estacion"=>'000',
            "sinimpuest"=>'0.0000000',
            "almacen"=>'01',
            "sector"=>'01',
            "formafis"=>'1',
            "al_libro"=>'1',
            "codigoret"=>'1',
            "retencion"=>'0.0000000',
            "aux1"=>'0.0000000',
            "aux2"=>'0.0000000',
            "aplicadoa"=>'00000001   ',
            "referencia"=>' ',
            "refmanual"=>' ',
            "doc_class_id"=>' ',
            "operac"=>'02',
            "motanul"=>' ',
            "seimporto"=>'1',
            "dbcr"=>'1',
            "horadocum"=>Carbon::now()->format('H:i'),
            "ampm"=>'2',
            "tranferido"=>'0',
            "procedecre"=>'0',
            "entregado"=>'0',
            "vuelto"=>'0.0000000',
            "integrado"=>'0',
            "escredito"=>'0',
            "seq_nodo"=>'0000000001',
            "tipo_nc"=>' ',
            "porbackord"=>'0',
            "chequedev"=>'0',
            "ordentrab"=>' ',
            "compcont"=>' ',
            "planillacob"=>' ',
            "nodoremoto"=>' ',
            "turno"=>' ',
            "codvend_a"=>' ',
            "codvend_b"=>' ',
            "codvend_C"=>' ',
            "codvend_d"=>' ',
            "baseimpo1"=>'0.0000000',
            "baseimpo2"=>'0.0000000',
            "baseimpo3"=>'0.0000000',
            "iddocumento"=>' ',
            "imp_nacional"=>'0.0000000',
            "imp_producc"=>'0.0000000',
            "retencioniva"=>'0.0000000',
            "fechayhora"=>Carbon::now(),
            "tipopersona"=>'0',
            "idvalidacion"=>' ',
            "nosujeto"=>'0',
            "serialprintf"=>' ',
            "documentofiscal"=>'...',
            "numeroz"=>' ',
            "ubica"=>' ',
            "usa_despacho"=>'0',
            "despachador"=>' ',
            "despacho_nro"=>' ',
            "checkin"=>' ',
            "nureserva"=>' ',
            "grandocnum"=>' ',
            "agenciant"=>' ',
            "tipodocant"=>' ',
            "documant"=>' ',
            "uemisorant"=>' ',
            "estacioant"=>' ',
            "emisionant"=>Carbon::now()->format('Y-m-d'),
            "fchyhrant"=>Carbon::now(),
            "frog"=>'0',
            "apa_nc"=>'0',
            "documentolocal"=>'...',
            "comanda_movil"=>'0',
            "comanda_kmonitor"=>'0',
            "para_llevar"=>'0',
            "notimbrar"=>'0',
            "antipo"=>' ',
            "antdoc"=>' ',
            "xrequest"=>' ',
            "xresponse"=>' ',
            "parcialidad"=>'0',
            "cedcompra"=>' ',
            "subcodigo"=>' ',
            "contingencia"=>'0',
            "precta_movil"=>'0',
            "tipodocfiscal"=>'0',
            "cprefijoserie"=>' ',
            "cserie"=>' ',
            "serieincluyeimpuesto"=>'0',
            "serieauto"=>'0',
            "opemail"=>' ',
            "refmanual2"=>' ',
          ]);

          // Cargoenc::firstOrCreate([
          //   "documento"=>$request->documento
          // ],[
          //   "id_empresa"=>'001000',
          //   "agencia"=>'001',
          //   "documento"=>$request->documento,
          //   "ordentrab"=>' ',
          //   "requisicio"=>' ',
          //   "cargado"=>'0',
          //   "status"=>'0',
          //   "realizador"=>"FCT",
          //   "emisor"=>'PREMIUM',
          //   "motivo"=>'PRUEBA',
          //   "fecha"=>Carbon::now()->format('Y-m-d'),
          //   "hora"=>Carbon::now()->format('H:i:s'),
          //   "estacion"=>'000',
          //   "tipoentradasalida"=>'01',
          //   "traex_nro"=>' ',
          //   "traex_replica"=>'0',
          // ]);

          /* INSERT PLATAFORMA WEB */
            DB::commit();
            return response()->json(["data"=>"Pedido enviado exitosamente."],200);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(["error"=>$e->getMessage()],500);
      }
    }

}

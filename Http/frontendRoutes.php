<?php

use Illuminate\Routing\Router;
$locale = LaravelLocalization::setLocale() ?: App::getLocale();
$router->get('getArticles', ['as' => 'article.show', 'uses' => 'PublicController@getArticles']);
$router->get('showCart', ['as' => 'show.cart', 'uses' => 'PublicController@showCart']);
$router->post('sendOrder', ['as' => 'store.cart', 'uses' => 'PublicController@storeCart']);
$router->get('apiOperti', ['as' => 'api.operti', 'uses' => 'PublicController@apiOperti']);
$router->get('allProducts', ['as' => 'show.products', 'uses' => 'PublicController@showAllProducts']);
$router->get('blog', ['as' => 'show.blog', 'uses' => 'PublicController@showBlog']);

<?php

return [
    'premiumsoft.databasehosts' => [
        'index' => 'premiumsoft::databasehosts.list resource',
        'create' => 'premiumsoft::databasehosts.create resource',
        'edit' => 'premiumsoft::databasehosts.edit resource',
        'destroy' => 'premiumsoft::databasehosts.destroy resource',
    ],

    'premiumsoft.priceadjustment' => [
        'index' => 'premiumsoft::priceadjustment.list resource',
        'create' => 'premiumsoft::priceadjustment.create resource',
        'edit' => 'premiumsoft::priceadjustment.edit resource',
        'destroy' => 'premiumsoft::priceadjustment.destroy resource',
    ],
    'premiumsoft.updateproductscodes' => [
        'index' => 'premiumsoft::updateproductscodes.list resource',
        'create' => 'premiumsoft::updateproductscodes.create resource',
        'edit' => 'premiumsoft::updateproductscodes.edit resource',
        'destroy' => 'premiumsoft::updateproductscodes.destroy resource',
    ],
// append


];

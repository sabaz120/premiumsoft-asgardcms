<?php

namespace Modules\Premiumsoft\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterPremiumsoftSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('Premiumsoft'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('Gestionar empresas'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.premiumsoft.databasehost.create');
                    $item->route('admin.premiumsoft.databasehost.index');
                    $item->authorize(
                        $this->auth->hasAccess('premiumsoft.databasehosts.index')
                    );
                });

                $item->item('Ajuste de precios', function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->route('admin.premiumsoft.priceadjustment.index');
                    $item->authorize(
                        $this->auth->hasAccess('premiumsoft.priceadjustment.index')
                    );
                });
                $item->item(trans('Actualizar Códigos'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.premiumsoft.updateproductscode.create');
                    $item->route('admin.premiumsoft.updateproductscode.index');
                    $item->authorize(
                        $this->auth->hasAccess('premiumsoft.updateproductscodes.index')
                    );
                });

                $item->item(trans('Gestionar clientes'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.premiumsoft.updateproductscode.create');
                    $item->route('admin.premiumsoft.client.index');
                    $item->authorize(
                        $this->auth->hasAccess('premiumsoft.updateproductscodes.index')
                    );
                });


                $item->item(trans('Gestionar pedidos'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.premiumsoft.updateproductscode.create');
                    $item->route('admin.premiumsoft.orders.index');
                    $item->authorize(
                        $this->auth->hasAccess('premiumsoft.updateproductscodes.index')
                    );
                });




// append


            });
        });

        return $menu;
    }
}

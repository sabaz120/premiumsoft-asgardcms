<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class DatabaseHost extends Model
{

    protected $table = 'premiumsoft__databasehosts';
    protected $fillable = [
    	"name",
    	"driver",
    	"ip",
    	"port",
    	"database",
    	"username",
    	"password",
    	"charset"
    ];
}

<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Ajustemv extends Model
{

  public $timestamps = false;
    // protected $connection = 'premium';
    protected $table = 'ajustemv';
    protected $primaryKey = 'codigo';
    protected $fillable = [
        "codigo",
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

}

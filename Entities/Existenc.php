<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Existenc extends Model
{

  public $timestamps = false;
    protected $connection = 'premiumsoft';
    protected $table = 'existenc';
    protected $primaryKey = 'codigo';
    protected $fillable = [
        "codigo"
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

}

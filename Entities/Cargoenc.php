<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Cargoenc extends Model
{
  public $timestamps = false;
    protected $connection = 'premiumsoft';
    protected $table = 'cargoenc';
    protected $primaryKey = 'documento';
    protected $fillable = [
      "id_empresa",
      "agencia",
      "documento",
      "ordentrab",
      "requisicio",
      "cargado",
      "status",
      "emisor",
      "motivo",
      "fecha",
      "hora",
      "estacion",
      "tipoentradasalida",
      "traex_nro",
      "traex_replica",
    ];

    protected $casts = [
        'documento' => 'string'
    ];
}

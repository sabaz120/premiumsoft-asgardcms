<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Cargodet extends Model
{

  public $timestamps = false;
    protected $connection = 'premiumsoft';
    protected $table = 'cargodet';
    protected $primaryKey = 'codigo';
    protected $fillable = [
      "id_empresa",
      "agencia",
      "documento", //El campo documento es el valor del campo documento que se guardo en el kardex (el que se creo en la vista)
      "status",
      "ordentrab",
      "codigo",
      "idcampo",
      "nombre",
      "grupo",
      "subgrupo",
      "proceso"=>'1',
      "cantidad",
      "tallas",
      "colores",
      "fecha",
      "almacen",
      "emisor",
      "estacion",
      "estacion",
      "usaserial",
      "seriales",
      "ubicacion",
      "precio",
      "fechanul",
      "useranul",
      "motivoanul",
      "lote",
      "costo",
      "costopromfecha",
      "factor",
      "tipo",
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

}

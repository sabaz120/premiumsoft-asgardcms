<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Gastarmv extends Model
{

  public $timestamps = false;
    protected $connection = 'premium';
    protected $table = 'gastarmv';
    protected $primaryKey = 'codigo';
    protected $fillable = [
        "codigo",
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

}

<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
public $timestamps = false;
    protected $connection = 'premiumsoft';
    protected $table = 'articulo';
    protected $primaryKey = 'codigo';
    protected $fillable = [
        "id_empresa",
        "agencia",
        "codigo",
        "grupo",
        "subgrupo",
        "nombre",
        "nombrecorto",
        "referencia",
        "marca",
        "unidad",
        "usaexist",
        "para_web",
        "costo",
        "precio1",
        "precio2",
        "precio3",
        "precio4",
        "precio5",
        "precio6",
        "precio7",
        "precio8",
        "preciofin1",
        "preciofin2",
        "preciofin3",
        "preciofin4",
        "preciofin5",
        "preciofin6",
        "preciofin7",
        "preciofin8",
        "existencia",
        "rotacionvta",
        "inactiva",
        "comprometido",
        "minimo",
        "maximo",
        "impuesto1",
        "impuesto2",
        "impuesto3",
        "impuesto4",
        "impuesto5",
    	"impuesto6",
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

    public function grupo(){
      return $this->belongsTo('Modules\Premiumsoft\Entities\Grupos','grupo');
    }


}

<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Kardex extends Model
{

  public $timestamps = false;
    protected $connection = 'premiumsoft';
    protected $table = 'kardex';
    protected $primaryKey = 'codigo';
    protected $fillable = [
      "id_empresa",
      "agencia",
      "documento",
      "codigo",
      "hora",
      "grupo",
      "origen",
      "concepto",
      "cantidad",
      "estacion",
      "almacen",
      "sumaresta",
      "emisor",
      "fecha",
      "aux1",
      "aux2",
      "aux3",
      "tipo_resta",
      "pid",
      "idvalidacion",
      "costo",
      "costoprom",
      "exist_ant",
      "costoprompbl",
      "costoex_adic",
      "incrcosprom",
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

}

<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Devolmv extends Model
{

  public $timestamps = false;
    protected $connection = 'premium';
    protected $table = 'devolmv';
    protected $primaryKey = 'codigo';
    protected $fillable = [
        "codigo",
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

}

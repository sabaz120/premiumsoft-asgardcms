<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
  protected $connection = 'premiumsoft';
  protected $table = 'grupos';
  protected $primaryKey = 'codigo';
  protected $fillable = ['codigo'];
  
  protected $casts = [
    'codigo' => 'string'
  ];
}

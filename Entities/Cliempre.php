<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Cliempre extends Model
{
    public $timestamps =false;
    protected $connection = 'premiumsoft';
    protected $table = 'cliempre';
    protected $primaryKey = 'codigo';
    protected $fillable = [
        "id_empresa",
        "agencia",
        "codigo",
        "nombre",
        "cedula",
        "nrorif",
        "nronit",
        "referenc1",
        "referenc2",
        "direccion",
        "telefonos",
        "telefono_movil",
        "numerofax",
        "fecha",
        "perscont",
        "limite",
        "dias",
        "precio",
        "status",
        "nota",
        "fechanac",
        "fechanac1",
        "fechanac2",
        "fechanac3",
        "fechanac4",
        "fechanac5",
    ];

    protected $casts = [
        'codigo' => 'string'
    ];
}

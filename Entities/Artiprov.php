<?php

namespace Modules\Premiumsoft\Entities;

use Illuminate\Database\Eloquent\Model;

class Artiprov extends Model
{

  public $timestamps = false;
    protected $connection = 'premiumsoft';
    protected $table = 'artiprov';
    protected $primaryKey = 'codigo';
    protected $fillable = [
        "codigo"
    ];

    protected $casts = [
        'codigo' => 'string'
    ];

}

<?php

namespace Modules\Premiumsoft\Repositories\Cache;

use Modules\Premiumsoft\Repositories\DatabaseHostRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDatabaseHostDecorator extends BaseCacheDecorator implements DatabaseHostRepository
{
    public function __construct(DatabaseHostRepository $databasehost)
    {
        parent::__construct();
        $this->entityName = 'premiumsoft.databasehosts';
        $this->repository = $databasehost;
    }
}

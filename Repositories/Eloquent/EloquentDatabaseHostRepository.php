<?php

namespace Modules\Premiumsoft\Repositories\Eloquent;

use Modules\Premiumsoft\Repositories\DatabaseHostRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDatabaseHostRepository extends EloquentBaseRepository implements DatabaseHostRepository
{
}

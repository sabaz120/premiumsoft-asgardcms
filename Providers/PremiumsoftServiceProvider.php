<?php

namespace Modules\Premiumsoft\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Premiumsoft\Events\Handlers\RegisterPremiumsoftSidebar;

class PremiumsoftServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterPremiumsoftSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('databasehosts', array_dot(trans('premiumsoft::databasehosts')));
            // append translations


        });
    }

    public function boot()
    {
        $this->publishConfig('premiumsoft', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Premiumsoft\Repositories\DatabaseHostRepository',
            function () {
                $repository = new \Modules\Premiumsoft\Repositories\Eloquent\EloquentDatabaseHostRepository(new \Modules\Premiumsoft\Entities\DatabaseHost());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Premiumsoft\Repositories\Cache\CacheDatabaseHostDecorator($repository);
            }
        );

// add bindings


    }
}

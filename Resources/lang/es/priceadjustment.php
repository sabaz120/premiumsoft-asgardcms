<?php

return [
    'list resource' => 'List de ajustes de precios',
    'create resource' => 'Crear ajuste de precio',
    'edit resource' => 'Editar ajuste de precio',
    'destroy resource' => 'Eliminar ajuste de precio',
    'title' => [
        'priceadjustment' => 'Ajuste de precio',
        'create priceadjustment' => 'Ajustes de precio',
        'edit priceadjustment' => 'Editar ajuste de precio',
    ],
    'button' => [
        'create priceadjustment' => 'Crear ajuste de precio',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];

<?php

return [
    'list resource' => 'List databasehosts',
    'create resource' => 'Create databasehosts',
    'edit resource' => 'Edit databasehosts',
    'destroy resource' => 'Destroy databasehosts',
    'title' => [
        'databasehosts' => 'DatabaseHost',
        'create databasehost' => 'Create a databasehost',
        'edit databasehost' => 'Edit a databasehost',
    ],
    'button' => [
        'create databasehost' => 'Create a databasehost',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];

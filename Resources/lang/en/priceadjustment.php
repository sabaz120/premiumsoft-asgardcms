<?php

return [
    'list resource' => 'List updateproductscodes',
    'create resource' => 'Create updateproductscodes',
    'edit resource' => 'Edit updateproductscodes',
    'destroy resource' => 'Destroy updateproductscodes',
    'title' => [
        'updateproductscodes' => 'UpdateProductsCode',
        'create updateproductscode' => 'Create a updateproductscode',
        'edit updateproductscode' => 'Edit a updateproductscode',
    ],
    'button' => [
        'create updateproductscode' => 'Create a updateproductscode',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];

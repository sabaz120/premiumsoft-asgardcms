<fieldset id="products">
  @include('premiumsoft::frontend.partials.navigation')
  <div class="offcanvas-wrapper">
    @include('partials.slider')
  <section class="container padding-top-3x padding-bottom-3x">
          <h3 class="text-center mb-30">Productos</h3>
          <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
            <!-- Product-->
            <div class="grid-item" v-for="(item,index) in products" >
              <div class="product-card">
                <a class="product-thumb" href="shop-single.html">
                  <img  :src="base_path+item.rutafoto" style="height:13rem">

                </a>
                <h3 class="product-title"><a href="shop-single.html">@{{item.nombre}}</a></h3>
                <h4 class="product-price">
                  <!-- Bs @{{item.precio1}} -->
                  <input type="text" name="" class="form-control" :value="item.precio1" v-money="money" disabled>
                </h4>
                <div class="product-buttons">
                  <button @click="addCart(item)" class="btn btn-outline-primary btn-sm"
                  data-toast data-toast-type="success" data-toast-position="topRight"
                  data-toast-icon="icon-circle-check"
                  :data-toast-title="item.nombre"
                  data-toast-message="agregado al carrito exitosamente">
                    Agregar al carrito
                  </button>
                </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class=" mx-auto">
                <a class="btn btn-outline-secondary  " href="{{route('show.products')}}">
                  Ver más productos
                </a>
              </div>
            </div>
          </div>

        </section>


        <!-- Product Widgets-->
        <section class="container padding-bottom-2x">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="widget widget-featured-products">
                <h3 class="widget-title">Top Sellers</h3>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src=" {{ Theme::url('img/shop/widget/01.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Oakley Kickback</a></h4><span class="entry-meta">$155.00</span>
                  </div>
                </div>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/03.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Vented Straw Fedora</a></h4><span class="entry-meta">$49.50</span>
                  </div>
                </div>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/04.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Big Wordmark Tote</a></h4><span class="entry-meta">$29.99</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="widget widget-featured-products">
                <h3 class="widget-title">New Arrivals</h3>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/05.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Union Park</a></h4><span class="entry-meta">$49.99</span>
                  </div>
                </div>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/06.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Cole Haan Crossbody</a></h4><span class="entry-meta">$200.00</span>
                  </div>
                </div>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/07.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Skagen Holst Watch</a></h4><span class="entry-meta">$145.00</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="widget widget-featured-products">
                <h3 class="widget-title">Best Rated</h3>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/08.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Jordan's City Hoodie</a></h4><span class="entry-meta">$65.00</span>
                  </div>
                </div>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/09.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Palace Shell Track Jacket</a></h4><span class="entry-meta">$36.99</span>
                  </div>
                </div>
                <!-- Entry-->
                <div class="entry">
                  <div class="entry-thumb"><a href="shop-single.html"><img src="{{ Theme::url('img/shop/widget/10.jpg ') }}" alt="Product"></a></div>
                  <div class="entry-content">
                    <h4 class="entry-title"><a href="shop-single.html">Off the Shoulder Top</a></h4><span class="entry-meta">$128.00</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Popular Brands-->
        <section class="bg-faded padding-top-3x padding-bottom-3x">
          <div class="container">
            <h3 class="text-center mb-30 pb-2">Popular Brands</h3>
            <div class="owl-carousel"
            data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: false, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2}, &quot;470&quot;:{&quot;items&quot;:3},&quot;630&quot;:{&quot;items&quot;:4},&quot;991&quot;:{&quot;items&quot;:5},&quot;1200&quot;:{&quot;items&quot;:6}} }"><img class="d-block w-110 opacity-75 m-auto" src="{{ Theme::url('img/brands/01.png') }}" alt="Adidas"><img class="d-block w-110 opacity-75 m-auto"
              src=" {{ Theme::url('img/brands/02.png') }}" alt="Brooks"><img class="d-block w-110 opacity-75 m-auto" src="{{ Theme::url('img/brands/03.png') }}" alt="Valentino"><img class="d-block w-110 opacity-75 m-auto" src="{{ Theme::url('img/brands/04.png') }}" alt="Nike"><img class="d-block w-110 opacity-75 m-auto" src="{{ Theme::url('img/brands/05.png') }}" alt="Puma"><img class="d-block w-110 opacity-75 m-auto" src="{{ Theme::url('img/brands/06.png') }}" alt="New Balance"><img class="d-block w-110 opacity-75 m-auto" src="{{Theme::url('img/brands/07.png') }}" alt="Dior"></div>
          </div>
        </section>
        <!-- Services-->
        <!-- <section class="container padding-top-3x padding-bottom-2x">
          <div class="row">
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="{{ Theme::url('img/services/01.png ') }}" alt="Shipping">
              <h6>Free Worldwide Shipping</h6>
              <p class="text-muted margin-bottom-none">Free shipping for all orders over $100</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="{{ Theme::url('img/services/02.png ') }}" alt="Money Back">
              <h6>Money Back Guarantee</h6>
              <p class="text-muted margin-bottom-none">We return money within 30 days</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="{{ Theme::url('img/services/03.png ') }}" alt="Support">
              <h6>24/7 Customer Support</h6>
              <p class="text-muted margin-bottom-none">Friendly 24/7 customer support</p>
            </div>
            <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="{{ Theme::url('img/services/04.png ') }}" alt="Payment">
              <h6>Secure Online Payment</h6>
              <p class="text-muted margin-bottom-none">We posess SSL / Secure Certificate</p>
            </div>
          </div>
        </section> -->
      </div>

</fieldset>

@section('scripts')

<script type="text/javascript">
const app=new Vue({
  el:"#products",

  data:{
    products:[],

    cart:[],
    monto:'200000',
    total:0,
    flagCart:false,
    base_path:'{{url('/')}}'+'/premium/',
    money: {
       decimal: '.',
       thousands: '.',
       prefix: 'Bs',
       suffix: '',
       precision: 0,
     },
  },

  mounted(){
    this.getArticles();
    this.cart =JSON.parse(localStorage.getItem('cart'));
    if (localStorage.getItem('cart') == null) {
      this.cart=[];
    }
      this.totalCart();
  },

  methods:{
    getArticles(){
      axios.get("{{route ('article.show')}}").then(response=>{
        this.products=response.data.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    addCart(product){
      product.cantidad=1;
      this.cart.push(product);
      this.totalCart();
      // this.cart.push({nombre:product.nombre,precio:product.precio1,grupo:product.grupo.nombre,codigoGrupo:product.grupo.codigo,existencia:product.existencia,codigoProducto:product.codigo})
      localStorage.setItem('cart', JSON.stringify(this.cart));
    },

    totalCart(){
      this.total =0;
      for (var i = 0; i < this.cart.length; i++) {
        this.total = this.total+parseInt(this.cart[i].precio1);
      }
    },
  }
});
</script>
@endsection

@extends('layouts.master')
@section('content')
<style media="screen">
 textarea{
   resize: none;
 }
</style>
<fieldset id="cart">

@include('premiumsoft::frontend.partials.navigation')
@include('partials.slider')
<div class="container padding-bottom-3x mb-1" >
  <!-- Shopping Cart-->
  <div class="table-responsive shopping-cart" v-if="cart.length>0">
    <table class="table">
      <thead>
        <tr>
          <th>Nombre del producto</th>
          <th class="text-center">Cantidad</th>
          <th class="text-center">Precio</th>
          <th class="text-center">Subtotal</th>
          <th class="text-center"><a class="btn btn-sm btn-outline-danger" @click="clearAll">Limpiar carrito</a></th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(item,index) in cart" >
          <td>
            <div class="product-item"><a class="product-thumb" href="shop-single.html">
              <img  :src="base_path+item.rutafoto" style="height:7rem">
            </a>
              <div class="product-info">
                <h4 class="product-title">
                  <a href="shop-single.html">@{{item.nombre}}</a>
                </h4>
                  <span><em>Categoría:</em>@{{item.grupo.nombre}}</span>
                  <!-- <span><em>Color:</em> Dark Blue</span> -->
              </div>
            </div>
          </td>
          <td class="text-center">
            <div class="count-input">
              <select class="form-control" v-model="item.cantidad" @change="subTotal">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
            </div>
          </td>
          <td class="text-center text-lg text-medium">
            <!-- Bs@{{item.precio1}} -->
            <input type="text"  class="form-control" :value="item.precio1" v-money="money" readonly>

          </td>
          <td class="text-center text-lg text-medium">
            <!-- Bs@{{item.cantidad*item.precio1}} -->
              <input type="text"  class="form-control" :value="item.cantidad*item.precio1" v-money="money" readonly>
          </td>

          <td class="text-center">
            <a class="remove-from-cart" @click="clearProduct(index)" data-toggle="tooltip" title="Eliminar producto">
              <i class="icon-cross"></i>
            </a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="pt-5 pb-5" v-else>
    <h3 class="text-center">El carrito de compras esta vacio</h3>
  </div>
  <div class="shopping-cart-footer">
    <div class="column text-lg">Total:
      <span class="text-medium">
        <!-- Bs @{{totalCart}} -->
        <input type="text" :value="totalCart" v-money="money" disabled class="form-control text-center col-2 ml-auto">
      </span>
    </div>
  </div>

  <div class="shopping-cart-footer">
    <div class="column"><a class="btn btn-outline-secondary" href="{{url('/')}}"><i class="icon-arrow-left"></i>&nbsp;Volver a comprar</a></div>
    <div class="column">
    <a class="btn btn-success" href="checkout-address.html"  v-show="cart.length>0"  data-toggle="modal" data-target="#modalCentered">Realizar pedido</a></div>
  </div>
</div>


<!-- Vertically Centered Modal-->
    <div class="modal fade" id="modalCentered" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content ">
          <div class="modal-header">
            <h4 class="modal-title">Realizar pedido</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label for="">Nombre</label>
                  <input class="form-control" type="text" v-model="nombre" placeholder="Pedro " >
                </div>
                <div class="col-sm-4 form-group">
                  <label for="">Cédula de identidad</label>
                  <input class="form-control" type="text" v-model="cedula" placeholder="V-12345678"  >
                </div>

                <div class="col-sm-4 form-group">
                  <label for="">N° de Teléfono</label>
                  <input class="form-control" type="number" v-model="telefono" placeholder="04121209060">
                </div>
              </div>
              <div class="row">

                <div class="col-sm-12 form-group">
                  <label for="">Dirección</label>
                  <textarea name="name" rows="4" cols="80" class="form-control" v-model="direccion"></textarea>
                </div>
              </div>

          </div>
          <div class="modal-footer">
            <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
            <button class="btn btn-primary btn-sm" type="button" @click="sendOrder">Enviar pedido</button>
          </div>
        </div>
      </div>
    </div>


</fieldset>

@stop
@section('scripts2')
<script type="text/javascript">

var app2=new Vue({
  el:"#cart",
  data:{
    cart:[],
    nombre:'',
    cedula:'',
    direccion:'',
    telefono:'',
    total:0,
    totalCart:0,
    flagCart:true,
    code:'',
    nums:[1,2,3,4,5,6,7,8,9,10],
    operti:[],
    base_path:'{{url('/')}}'+'/premium/',
    money: {
       decimal: '.',
       thousands: '.',
       prefix: 'Bs',
       suffix: '',
       precision: 0,
     },
    // operti:{!! $operti !!},

  },
  mounted(){
    this.cart =JSON.parse(localStorage.getItem('cart'));
    if (localStorage.getItem('cart') == null) {
      this.cart=[];
    }
      this.totalNav();
      this.subTotal();
      this.codeGenerate();
      // this.getOperti();
  },
  methods:{
    getOperti(){
      axios.get("{{route ('api.operti')}}").then(response=>{
        this.operti=response.data.data;
        console.log(response.data.data);
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    clearAll(){
      localStorage.removeItem('cart');
      this.cart=[];
      localStorage.removeItem('cart');
    },
    clearProduct(index){
      this.cart.splice(index,1);
      localStorage.setItem('cart', JSON.stringify(this.cart));
    },
    subTotal(){
      this.totalCart =0;
      for (var i = 0; i < this.cart.length; i++) {
        this.totalCart = this.totalCart+parseFloat(this.cart[i].precio1*this.cart[i].cantidad);
      }
    },
    codeGenerate(){
       this.code = 'W'+Math.floor(Math.random() * 26) + Date.now();
       // var correlativo = '00000'+this.operti.length;
       // var suma = parseInt(correlativo)+1;
       // this.code='W00000'+suma;

       this.code= this.code.substr(0,7)
    },
    sendOrder: function(){
      axios.post("{{ route('store.cart')}}",
      {
        documento:this.code,nombre:this.nombre,cedula:this.cedula,telefono:this.telefono,
        direccion:this.direccion,totalCarrito:this.totalCart,articulos:this.cart
      }).then(response => {
          alert(response.data.data)
          $('#modalCentered').modal('toggle');
          this.cart=[];
          this.nombre="";
          this.cedula="";
          this.telefono="";
          this.direccion="";
          this.total="";
          this.totalCart="";
          this.getOperti();
          this.codeGenerate();
          this.clearAll();
      }).catch(error => {
        console.log(error);
      });
    },
    totalNav(){
      this.total =0;
      for (var i = 0; i < this.cart.length; i++) {
        this.total = this.total+parseInt(this.cart[i].precio1);
      }
    },



  }
});
</script>
@endsection

@extends('layouts.master')
@section('content')
<style media="screen">
 textarea{
   resize: none;
 }
</style>
<fieldset id="allProducts">

@include('premiumsoft::frontend.partials.navigation')
<div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Productos</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="{{url('/')}}">Inicio</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Productos</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-1">
        <div class="row">
          <!-- Products-->
          <div class="col-xl-9 col-lg-8 order-lg-2">
            <!-- Shop Toolbar-->
            <!-- <div class="shop-toolbar padding-bottom-1x mb-2">
              <div class="column">
                <div class="shop-sorting">
                  <label for="sorting">Sort by:</label>
                  <select class="form-control" id="sorting">
                    <option>Popularity</option>
                    <option>Low - High Price</option>
                    <option>High - Low Price</option>
                    <option>Avarage Rating</option>
                    <option>A - Z Order</option>
                    <option>Z - A Order</option>
                  </select><span class="text-muted">Showing:&nbsp;</span><span>1 - 12 items</span>
                </div>
              </div>
              <div class="column">
                <div class="shop-view"><a class="grid-view active" href="shop-grid-ls.html"><span></span><span></span><span></span></a><a class="list-view" href="shop-list-ls.html"><span></span><span></span><span></span></a></div>
              </div>
            </div> -->
            <!-- Products Grid-->
            <div class="isotope-grid cols-3 mb-2">
              <div class="gutter-sizer"></div>
              <div class="grid-sizer"></div>
              <!-- Product-->
              <div class="grid-item" v-for="(items,index) in products">
                <div class="product-card"><a class="product-thumb" href="shop-single.html">
                  <img  :src="base_path+items.rutafoto" style="height:10rem">
                </a>
                  <h3 class="product-title">
                      @{{items.nombre}}
                  </h3>
                  <h4 class="product-price">Bs @{{items.precio1}}</h4>
                  <div class="product-buttons">
                    <!-- <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button> -->
                    <button @click="addCart(items)" class="btn btn-outline-primary btn-sm"
                    data-toast data-toast-type="success" data-toast-position="topRight"
                    data-toast-icon="icon-circle-check"
                    :data-toast-title="items.nombre"
                    data-toast-message="agregado al carrito exitosamente">
                      Agregar al carrito
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Pagination-->
            <!-- <nav class="pagination">
              <div class="column">
                <ul class="pages">
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li>...</li>
                  <li><a href="#">12</a></li>
                </ul>
              </div>
              <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
            </nav> -->
          </div>
          <!-- Sidebar          -->
          <div class="col-xl-3 col-lg-4 order-lg-1">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
            <aside class="sidebar sidebar-offcanvas">
              <!-- Widget Categories-->
              <section class="widget widget-categories">
                <h3 class="widget-title">Shop Categories</h3>
                <ul>
                  <li class="has-children expanded"><a href="#">Shoes</a><span>(1138)</span>
                    <ul>
                      <li><a href="#">Women's</a><span>(508)</span>
                        <ul>
                          <li><a href="#">Sneakers</a></li>
                          <li><a href="#">Heels</a></li>
                          <li><a href="#">Loafers</a></li>
                          <li><a href="#">Sandals</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Men's</a><span>(423)</span>
                        <ul>
                          <li><a href="#">Boots</a></li>
                          <li><a href="#">Oxfords</a></li>
                          <li><a href="#">Loafers</a></li>
                          <li><a href="#">Sandals</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Boy's Shoes</a><span>(97)</span></li>
                      <li><a href="#">Girl's Shoes</a><span>(110)</span></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Clothing</a><span>(2356)</span>
                    <ul>
                      <li><a href="#">Women's</a><span>(1032)</span>
                        <ul>
                          <li><a href="#">Dresses</a></li>
                          <li><a href="#">Shirts &amp; Tops</a></li>
                          <li><a href="#">Swimwear</a></li>
                          <li><a href="#">Shorts</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Men's</a><span>(937)</span>
                        <ul>
                          <li><a href="#">Shirts &amp; Tops</a></li>
                          <li><a href="#">Shorts</a></li>
                          <li><a href="#">Swimwear</a></li>
                          <li><a href="#">Pants</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Kid's Clothing</a><span>(386)</span></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Bags</a><span>(420)</span>
                    <ul>
                      <li><a href="#">Handbags</a><span>(180)</span></li>
                      <li><a href="#">Backpacks</a><span>(132)</span></li>
                      <li><a href="#">Wallets &amp; Accessories</a><span>(47)</span></li>
                      <li><a href="#">Luggage</a><span>(61)</span></li>
                    </ul>
                  </li>
                  <li class="has-children"><a href="#">Accessories</a><span>(874)</span>
                    <ul>
                      <li><a href="#">Sunglasses</a><span>(211)</span></li>
                      <li><a href="#">Hats</a><span>(195)</span></li>
                      <li><a href="#">Watches</a><span>(159)</span></li>
                      <li><a href="#">Jewelry</a><span>(203)</span></li>
                      <li><a href="#">Belts</a><span>(106)</span></li>
                    </ul>
                  </li>
                </ul>
              </section>
              <!-- Widget Price Range-->
              <!-- <section class="widget widget-categories">
                <h3 class="widget-title">Price Range</h3>
                <form class="price-range-slider" method="post" data-start-min="250" data-start-max="650" data-min="0" data-max="1000" data-step="1">
                  <div class="ui-range-slider"></div>
                  <footer class="ui-range-slider-footer">
                    <div class="column">
                      <button class="btn btn-outline-primary btn-sm" type="submit">Filter</button>
                    </div>
                    <div class="column">
                      <div class="ui-range-values">
                        <div class="ui-range-value-min">$<span></span>
                          <input type="hidden">
                        </div>&nbsp;-&nbsp;
                        <div class="ui-range-value-max">$<span></span>
                          <input type="hidden">
                        </div>
                      </div>
                    </div>
                  </footer>
                </form>
              </section> -->
              <!-- Widget Brand Filter-->
              <!-- <section class="widget">
                <h3 class="widget-title">Filter by Brand</h3>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="adidas">
                  <label class="custom-control-label" for="adidas">Adidas&nbsp;<span class="text-muted">(254)</span></label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="bilabong">
                  <label class="custom-control-label" for="bilabong">Bilabong&nbsp;<span class="text-muted">(39)</span></label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="klein">
                  <label class="custom-control-label" for="klein">Calvin Klein&nbsp;<span class="text-muted">(128)</span></label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="nike">
                  <label class="custom-control-label" for="nike">Nike&nbsp;<span class="text-muted">(310)</span></label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="bahama">
                  <label class="custom-control-label" for="bahama">Tommy Bahama&nbsp;<span class="text-muted">(42)</span></label>
                </div>
              </section> -->
              <!-- Widget Size Filter-->
              <!-- <section class="widget">
                <h3 class="widget-title">Filter by Size</h3>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="xl">
                  <label class="custom-control-label" for="xl">XL&nbsp;<span class="text-muted">(208)</span></label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="l">
                  <label class="custom-control-label" for="l">L&nbsp;<span class="text-muted">(311)</span></label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="m">
                  <label class="custom-control-label" for="m">M&nbsp;<span class="text-muted">(485)</span></label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="s">
                  <label class="custom-control-label" for="s">S&nbsp;<span class="text-muted">(213)</span></label>
                </div>
              </section> -->
              <!-- Promo Banner-->
              <section class="promo-box" style="background-image: url({{ Theme::url('img/banners/02.jpg  ') }});">
                <!-- Choose between .overlay-dark (#000) or .overlay-light (#fff) with default opacity of 50%. You can overrride default color and opacity values via 'style' attribute.--><span class="overlay-dark" style="opacity: .45;"></span>
                <div class="promo-box-content text-center padding-top-3x padding-bottom-2x">
                  <h4 class="text-light text-thin text-shadow">Publicidad</h4>
                  <h3 class="text-bold text-light text-shadow">Ofertas</h3><a class="btn btn-sm btn-primary" href="#">Ver</a>
                </div>
              </section>
            </aside>
          </div>
        </div>
      </div>
</fieldset>

@stop
@section('scripts2')
<script type="text/javascript">

var app3=new Vue({
  el:"#allProducts",
  data:{
    cart:[],
    products:[],
    base_path:'{{url('/')}}'+'/premium/',
    flagCart:false,
  },
  mounted(){
    this.getArticles();
    this.cart =JSON.parse(localStorage.getItem('cart'));
    if (localStorage.getItem('cart') == null) {
      this.cart=[];
    }
      this.totalCart();
  },
  methods:{
    getArticles(){
      axios.get("{{route ('article.show')}}").then(response=>{
        this.products=response.data.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    addCart(product){
      product.cantidad=1;
      this.cart.push(product);
      this.totalCart();
      localStorage.setItem('cart', JSON.stringify(this.cart));
    },
    totalCart(){
      this.total =0;
      for (var i = 0; i < this.cart.length; i++) {
        this.total = this.total+parseInt(this.cart[i].precio1);
      }
    },
  }
});
</script>
@endsection

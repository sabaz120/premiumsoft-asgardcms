@extends('layouts.master')

@section('content-header')
    <h1>
        Gestionar Pedidos
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">Gestionar Pedidos</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Referencia</th>
                                <th>C.I Cliente</th>
                                <th>Cliente</th>
                                <th>Total</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($orders as $order): ?>
                                <tr>
                                  <td>{{$order->documento}}</td>
                                  <td>{{$order->codcliente}}</td>
                                  <td>{{$order->nombrecli}}</td>
                                  <td>Bs {{$order->totcosto}}</td>
                                  <td>
                                    <button type="button" name="button" class="btn btn-primary">Procesar</button>
                                    <button type="button" name="button" class="btn btn-danger">Rechazar</button>
                                  </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                  <th>Referencia</th>
                                  <th>C.I Cliente</th>
                                  <th>Cliente</th>
                                  <th>Total</th>
                                  <th>Acción</th>
                              </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@stop


@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.premiumsoft.databasehost.create') ?>" }
                ]
            });
        });
    </script>

@endpush

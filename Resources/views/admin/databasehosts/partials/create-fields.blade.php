<div class="box-body row">

	<div class="col-md-4">
		<label>Driver</label>
		<select class="form-control" name="driver">
			<option value="mysql">MYSQL</option>
			<option value="pgsql">PGSQL</option>
		</select>
	</div>

	<div class="col-md-4">
		<label>IP</label>
		<input class="form-control" name="ip" placeholder="127.0.0.1" value="127.0.0.1"></input>
	</div>

	<div class="col-md-4">
		<label>Puerto</label>
		<input class="form-control" name="port" placeholder="3306" value=""></input>
	</div>

	<div class="col-md-4">
		<label>Nombre</label>
		<input class="form-control" name="name" placeholder="Nombre de conexión" value=""></input>
	</div>

	<div class="col-md-4">
		<label>Database</label>
		<input class="form-control" name="database" placeholder="" value=""></input>
	</div>

	<div class="col-md-4">
		<label>Username</label>
		<input class="form-control" name="username" placeholder="" value=""></input>
	</div>

	<div class="col-md-4">
		<label>Password</label>
		<input class="form-control" name="password" placeholder="" value=""></input>
	</div>

</div>

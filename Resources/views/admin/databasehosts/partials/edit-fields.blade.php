<div class="box-body row">

	<div class="col-md-4">
		<label>Driver</label>
		<select class="form-control" name="driver">
			<option value="mysql"  @if($databasehost->driver=='mysql') selected @endif >MYSQL</option>
			<option value="pgsql" @if($databasehost->driver=='pgsql') selected @endif>PGSQL</option>
		</select>
	</div>

	<div class="col-md-4">
		<label>IP</label>
		<input class="form-control" name="ip" placeholder="127.0.0.1" value="{{$databasehost->ip}}"></input>
	</div>

	<div class="col-md-4">
		<label>Puerto</label>
		<input class="form-control" name="port" placeholder="3306" value="{{$databasehost->port}}"></input>
	</div>

	<div class="col-md-4">
		<label>Nombre</label>
		<input class="form-control" name="name" placeholder="Nombre de conexión" value="{{$databasehost->name}}"></input>
	</div>

	<div class="col-md-4">
		<label>Database</label>
		<input class="form-control" name="database" placeholder="" value="{{$databasehost->database}}"></input>
	</div>

	<div class="col-md-4">
		<label>Username</label>
		<input class="form-control" name="username" placeholder="" value="{{$databasehost->username}}"></input>
	</div>

	<div class="col-md-4">
		<label>Password</label>
		<input class="form-control" name="password" placeholder="" value="{{$databasehost->password}}"></input>
	</div>

</div>

<link rel="stylesheet" href="{{url('modules/premiumsoft/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{url('modules/premiumsoft/css/loadingModal.css')}}">
<link rel="stylesheet" href="{{url('modules/premiumsoft/css/toastr.min.css')}}">

<div class="box-body row">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Seleccionar la empresa donde sea buscar el articulo</label>
      <select class="form-control" name="database_id" id="database_id">
        @foreach($database as $items)
        <option value="{{$items->id}}">{{$items->name}}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group col-md-6">
      <label for="">Código de articulo</label>
      <div class="input-group">
        <input type="search" class="form-control" name="code" id="code">
        <span class="input-group-btn">
          <button class="btn btn-primary" type="button" onclick="getProduct()">
            <span class="glyphicon glyphicon-search" aria-hidden="true" ></span>
      </button>
      </div>
    </div>
  </div> <!--form-row-->

  <div class="form-row" id="sectionUpdate">
    <div class="form-group col-md-4">
      <label for="">Seleccione la empresa donde se van actualizar los precios:</label><br>
      <select class="selectpicker" multiple id="arrayIds" name="arrayIds" data-title="Seleccione" >
        @foreach($database as $items)
        <option value="{{$items->id}}">{{$items->name}}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group col-md-4">
      <label for="">Precio neto</label>
      <input type="number" name="precio1" id="precio1" class="form-control" >
    </div>

    <div class="form-group col-md-4">
      <label for="">Costo</label>
      <input type="number" name="costo" id="costo" class="form-control"  >
    </div>

    <div class="button col-md-offset-5" style="padding-top:7rem;">
      <button type="button" name="button" class="btn btn-primary" onclick="priceUpdate()">Actualizar</button>
    </div>
  </div>
<br><br><br>

<div id="divProduct" class="table" style="padding-top:10rem !important;">
  <h3 class="col-md-offset-5" style="padding-bottom:5rem !important;">Listado de productos</h3>
  <table id="tableProduct" class="data-table table table-bordered table-hover">
    <thead>
      <tr>
        <th>Código</th>
        <th>Nombre</th>
        <th>Precio neto</th>
        <th>Costo actual</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>


<!-- Modal Loading-->
<div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="loadingModalLabel" aria-hidden="true">
  <div class="modal-dialog loadingDialog" role="document">
    <div class="modal-content loadingContent">
      <div class="modal-body loadingBody">
        <h2 class="loading-message text-center">Por favor espere</h2>
        <h4 class="text-center pt-3">Se está procesando su solicitud</h4>
        <hr>
        <div class="flower-spinner">
          <span class="loading">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </span>
        </div>

      </div>

    </div>
  </div>
</div>

</div>

<script src="{{url('modules/premiumsoft/js/bootstrap-select.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/premiumsoft/js/toastr.min.js')}}" charset="utf-8"></script>

<script type="text/javascript">
function getProduct(){
  $('#loadingModal').show().modal({backdrop: 'static', keyboard: false})
  $.ajax({
    url:"{{route('admin.premiumsoft.article.show')}}",
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{
      database_id:$("#database_id").val(),
      filters:{
        code:$("#code").val()
      }
  },
    success:function(result){
      $('#loadingModal').modal('toggle');
      var product = result.data;
      var html="";
      for(var i=0;i<product.length;i++){
        html+="<tr>";
        html+='<td>'+product[i].codigo+'</td>';
        html+='<td>'+product[i].nombre+'</td>';
        html+='<td>'+product[i].precio1+'</td>';
        html+='<td>'+product[i].costo+'</td>';
        html+='<td><button type="button" class="btn btn-primary btn-flat" onclick="showPrice('+product[i].precio1+','+product[i].preciofin1+','+product[i].costo+')"><i class="fa fa-edit"></i> </button> </td>';
        html+="</tr>";
      }//for
      if ( $.fn.DataTable.isDataTable('#tableProduct') ) {
        $('#tableProduct').DataTable().destroy();
      }
      $('#tableProduct tbody').html(html);
      $('#tableProduct').DataTable({
        dom: 'Bfrtip',
        "paginate": true,
        "lengthChange": true,
        "filter": true,
        "sort": true,
        "info": true,
        "autoWidth": true,
        "order": [[ 0, "desc" ]],
        "language": {
        }
      });
      $('#divProduct').show()
    },
    error:function(error){
      $('#loadingModal').modal('toggle');
    }
  });//ajax
}

function showPrice(precio1,preciofin1,costo){
  $("#precio1").val(precio1);
  $("#preciofin1").val(preciofin1);
  $("#costo").val(costo);
  $('#sectionUpdate').show();
}


function priceUpdate(){
  if ($("#arrayIds").val() ==null) {
    toastr.error("Debe seleccionar la empresa donde va actualizar el registro");
  }else {
    $('#loadingModal').show().modal({backdrop: 'static', keyboard: false})
  $.ajax({
    url:"{{route('admin.premiumsoft.price.update.update')}}",
    type:'POST',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{
      'precio1':$("#precio1").val(),
      'preciofin1':$("#precio1").val(),
      'costo':$("#costo").val(),
      'code':$("#code").val(),
      'arrayIds':$("#arrayIds").val(),
  },
    success:function(result){
      $('#loadingModal').modal('toggle');
      toastr.success('Registros actualizado exitosamente.');
      $("#precio1").val('');
      $("#preciofin1").val('');
      $("#costo").val('');
      $("#code").val('');
      $("#arrayIds").val('0');
      $('#sectionUpdate').hide();
      $('#divProduct').hide();
    },
    error:function(error){
      $('#loadingModal').modal('toggle');
    }
  });//ajax
  }

}



$(function() {
    $('#sectionUpdate').hide();
    $('#divProduct').hide();
});

</script>

@extends('layouts.master')

@section('content-header')
    <h1>
        Gestionar clientes
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">Gestionar clientes</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>C.I</th>
                                <th>Nombre</th>
                                <th>Teléfono</th>
                                <th>Dirección</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($clientes as $cliente): ?>
                                <tr>
                                  <td>{{$cliente->cedula}}</td>
                                  <td>{{$cliente->nombre}}</td>
                                  <td>{{$cliente->telefono}}</td>
                                  <td>{{$cliente->direccion}}</td>
                                  <td>
                                    <a href="https://api.whatsapp.com/send?phone='584164641388'&text=No pierdas nuestras ofertas del día de los enamorados">
                                      Comunicarse
                                    </a>
                                  </td>

                                </tr>
                              <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                              <th>C.I</th>
                              <th>Nombre</th>
                              <th>Teléfono</th>
                              <th>Dirección</th>
                              <th>Acción</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@stop


@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.premiumsoft.databasehost.create') ?>" }
                ]
            });
        });
    </script>

@endpush
